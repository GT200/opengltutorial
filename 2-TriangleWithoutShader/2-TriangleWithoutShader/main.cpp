#include "OpenGLWindow.h"
#include "SimpleStaticShape.h"
#include "VertexArray.h"
#include "VertexBuffer.h"

int main()
{
	OpenGLWindow myOpengGLWindow;
	myOpengGLWindow.initializeGLFW();
	myOpengGLWindow.windowHint();
	myOpengGLWindow.createOpenGlWindow("My first window");
	myOpengGLWindow.initializeSceneColor();
	myOpengGLWindow.initializeGlew();//glew is nedeed for using higher level of opengl
	//triangle vertices
	SimpleStaticShape staticShape;
	const std::vector<glm::vec3> triangle0vertices = staticShape.getTrianlge0Vertices();
	
	//VAO (create once and use attributes to tell open gl wich buffer container which data (vertex buffer, color buffer,...) 
	VertexArray va;
	va.bind();

	//VB0 triangle 0
	VertexBuffer VBOTraingle0(triangle0vertices);


    while (glfwGetKey(myOpengGLWindow.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(myOpengGLWindow.window)) {
		myOpengGLWindow.clearColorBuffer(); 
		//


		VBOTraingle0.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
		glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);

		// Swap buffers
		glfwSwapBuffers(myOpengGLWindow.window);
		glfwPollEvents();

    }

    glfwTerminate();

    return 0;



	




	return 0;
}