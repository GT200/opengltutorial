#include "VertexArray.h"

VertexArray::VertexArray() 
{
	glGenVertexArrays(1, &this->vertexArrayID);
}
VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &this->vertexArrayID);
}
void VertexArray::bind() 
{
	glBindVertexArray(this->vertexArrayID);
}
void VertexArray::unbind()
{
	glBindVertexArray(0);
}
