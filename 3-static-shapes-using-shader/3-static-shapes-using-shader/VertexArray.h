#pragma once
// Include GLEW. Always include it before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class VertexArray
{
	public:
		VertexArray();
		~VertexArray();
		GLuint vertexArrayID;
		void bind();
		void unbind();
};

