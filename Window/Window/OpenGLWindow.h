#pragma once
#include <GLFW/glfw3.h>
#include <stdio.h>
class OpenGLWindow
{
public:
	OpenGLWindow();
	int initializeGLFW();
	void windowHint();
	int createOpenGlWindow(const char* title);
	void initializeSceneColor();
	void clearColorBuffer();
	void clearDepthBuffer();
	void updateScene();

	GLFWwindow* window;


};

