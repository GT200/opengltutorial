#include "OpenGLWindow.h"
OpenGLWindow::OpenGLWindow()
{

}
int OpenGLWindow::initializeGLFW()
{
    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        return -1;
    }
    else {
        fprintf(stderr, "SUCESS in Initializing GLFW\n");
        return 1;
    }
}
void OpenGLWindow::windowHint()
{
    glfwWindowHint(GLFW_SAMPLES, 4); //antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //version core

}
int OpenGLWindow::createOpenGlWindow(const char* title)
{
    int width;
    int height;

    //On r�cup�re les dimensions de l'�cran pour cr�er la fen�tre
    GLFWmonitor* primary = glfwGetPrimaryMonitor();
    glfwGetMonitorWorkarea(primary, nullptr, nullptr, &width, &height);

    //Enfin on cr�e la fen�tre
    this->window = glfwCreateWindow(width, height, title, NULL, NULL);
    //glfwSwapInterval(1);
    //On v�rifie que l'initialisation a bien march�
    if (this->window == NULL) {
        fprintf(stderr, "Erreur lors de la cr�ation de la f�n�tre\n");
        glfwTerminate();
        return -1;
    }
    //Enfin on d�finit la fen�tre cr��e comme la fen�tre sur laquelle on va dessiner
    glfwMakeContextCurrent(this->window);

}

void OpenGLWindow::initializeSceneColor()
{
    glClearColor(0, 0.5f, 0.0f, 1.0f);
}

void OpenGLWindow::clearColorBuffer()
{
    glClear(GL_COLOR_BUFFER_BIT);
}
void OpenGLWindow::clearDepthBuffer()
{
    glClear(GL_DEPTH_BUFFER_BIT);
}

void OpenGLWindow::updateScene()
{
    //if (keyPressedOnce(GLFW_KEY_ESCAPE)) {
    //    closeWindow();
    //}
}
