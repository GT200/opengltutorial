#include "OpenGLWindow.h"

int main()
{
	OpenGLWindow myOpengGLWindow;
	myOpengGLWindow.initializeGLFW();
	myOpengGLWindow.windowHint();
	myOpengGLWindow.createOpenGlWindow("My first window");
	myOpengGLWindow.initializeSceneColor();

    while (glfwGetKey(myOpengGLWindow.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(myOpengGLWindow.window)) {
		myOpengGLWindow.clearColorBuffer(); 

        // Swap buffers
        glfwSwapBuffers(myOpengGLWindow.window);
        glfwPollEvents();

    }

    glfwTerminate();

    return 0;



	




	return 0;
}