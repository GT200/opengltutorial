﻿#include "OpenGLWindow.h"
#include "SimpleStaticShape.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "shader.h"

int main()
{
	OpenGLWindow myOpengGLWindow;
	myOpengGLWindow.initializeGLFW();
	myOpengGLWindow.windowHint();
	myOpengGLWindow.createOpenGlWindow("My first window");
	myOpengGLWindow.initializeSceneColor();
	myOpengGLWindow.initializeGlew();//glew is nedeed for using higher level of opengl
	// vertices
	SimpleStaticShape staticShape;
	const std::vector<glm::vec3> triangle0vertices = staticShape.getTrianlge0Vertices();
	const std::vector<glm::vec3> Suaqre0vertices = staticShape.getSquare0Vertices();

	//Color
	const std::vector<glm::vec3> triangle0Colors = staticShape.getTriangle0Colors();
	const std::vector<glm::vec3> square0Colors = staticShape.getSquare0Colors();



	Shader shader("./shaders/shader.vertex", "./shaders/shader.fragment");
	shader.Bind();


	//VAO (create once and use attributes to tell open gl wich buffer container which data (vertex buffer, color buffer,...) 
	VertexArray va;
	va.bind();

	//VB0 creation for position
	VertexBuffer VBOTraingle0(triangle0vertices);
	VertexBuffer VBOSquare0(Suaqre0vertices);

	//VBO creation for color
	VertexBuffer VBOtriangle0Colors0(triangle0Colors);
	VertexBuffer VBOsquare0Colors(square0Colors);


    while (glfwGetKey(myOpengGLWindow.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(myOpengGLWindow.window)) {
		myOpengGLWindow.clearColorBuffer(); 
		//

		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		VBOTraingle0.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
		glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);
		VBOTraingle0.unbind();

		VBOSquare0.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
		//glDrawArrays(GL_TRIANGLES, 0, 3);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);
		VBOSquare0.unbind();


		//===================COLOR
		VBOtriangle0Colors0.bind(1);//attribute 0 on gpu locaiton will be the vertex 3D location
		VBOtriangle0Colors0.unbind();

		VBOsquare0Colors.bind(1);//attribute 0 on gpu locaiton will be the vertex 3D location
		VBOsquare0Colors.unbind();


		// Swap buffers
		glfwSwapBuffers(myOpengGLWindow.window);
		glfwPollEvents();

    }

    glfwTerminate();

    return 0;

}