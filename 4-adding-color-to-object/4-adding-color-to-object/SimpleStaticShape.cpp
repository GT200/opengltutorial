#include "SimpleStaticShape.h"

SimpleStaticShape::SimpleStaticShape()
{

}
const std::vector<glm::vec3> SimpleStaticShape::getTrianlge0Vertices()
{
	return { glm::vec3(-0.4f, 0.1f, 0.0f), 
			glm::vec3(0.4f, 0.1f, 0.0f), 
			glm::vec3(0.0f, 0.7f, 0.0f) };
}

const std::vector<glm::vec3> SimpleStaticShape::getSquare0Vertices()
{
	return { glm::vec3(-0.2f, -0.1f, 0.0f),
		glm::vec3(-0.2f, -0.6f, 0.0f), 
		glm::vec3(0.2f, -0.1f, 0.0f), 
		glm::vec3(0.2f, -0.6f, 0.0f) };
}

const std::vector<glm::vec3> SimpleStaticShape::getSquare0Colors()
{
	return  { glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(1.0f, 0.5f, 0.0f) };
}

const std::vector<glm::vec3> SimpleStaticShape::getTriangle0Colors()
{
	return  { glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, 1.0f) };
}

