#include "VertexBuffer.h"
#include <iostream>
VertexBuffer::VertexBuffer(const std::vector<glm::vec3>& verticesData)
{

	this->nb_vertices = verticesData.size();
	this->bitSizeOfverticesData = sizeof(glm::vec3) * this->nb_vertices;
	glGenBuffers(1, &this->vetexBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, this->vetexBufferId);
	glBufferData(GL_ARRAY_BUFFER, this->bitSizeOfverticesData,&verticesData[0], GL_STATIC_DRAW);

}
VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &this->vetexBufferId);
}
void VertexBuffer::bind(int BufferIndexAttributForGPU)
{
	glEnableVertexAttribArray(BufferIndexAttributForGPU);
	glBindBuffer(GL_ARRAY_BUFFER, this->vetexBufferId);
	glVertexAttribPointer(BufferIndexAttributForGPU, 3, GL_FLOAT,GL_FALSE, 0, (void*)0);
}
void VertexBuffer::unbind()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLuint VertexBuffer::getNbVertices()
{
	return this->nb_vertices;
}