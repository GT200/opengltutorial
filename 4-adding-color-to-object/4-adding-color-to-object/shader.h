#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>


#include <string>
#include <unordered_map>


class Shader
{
public:
    Shader(std::string VertexShaderFilePath, std::string FragmentShaderFilePath);
    ~Shader();

    void Bind() const;
    void Unbind() const;
    unsigned int getShaderProgramID() const;
private:
    GLuint LoadShaders(const std::string vertex_file_path, const std::string fragment_file_path);
    std::string m_VertexShaderFilePath;
    std::string m_FragmentShaderFilePath;
    unsigned int shaderProgramID;
};