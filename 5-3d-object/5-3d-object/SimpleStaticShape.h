#pragma once
#include <glm/glm.hpp>
#include <vector>



namespace static_3d_geometry
{
    // A simple cube with unit size
    extern std::vector<glm::vec3> cubeVertices;
    extern glm::vec2 cubeTexCoords[6];
    extern std::vector<glm::vec3> cubeFaceColors;

    // A simple pyramid with unit size
    extern  std::vector < glm::vec3> pyramidVertices;
    extern glm::vec2 pyramidTexCoords[3];
    extern std::vector < glm::vec3> pyramidFaceColors;

    // Ground, that has greenish color
    extern glm::vec3 plainGroundVertices[4];
    extern glm::vec2 plainGroundTexCoords[4];
    extern glm::vec3 plainGroundColors[4];

    // 2D quad of unit size, anchored in lower-left point (same as texture coordinates, render with triangle strip)
    extern glm::vec2 quad2D[4];
}
