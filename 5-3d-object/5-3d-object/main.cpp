﻿#include "OpenGLWindow.h"
#include "SimpleStaticShape.h"
#include "VertexArray.h"
#include "VertexBuffer.h"

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>

int main()
{
	OpenGLWindow myOpengGLWindow;
	myOpengGLWindow.initializeGLFW();
	myOpengGLWindow.windowHint();
	myOpengGLWindow.createOpenGlWindow("My first window");
	myOpengGLWindow.initializeSceneColor();
	myOpengGLWindow.initializeGlew();//glew is nedeed for using higher level of opengl
	// vertices
	const std::vector<glm::vec3> cube = static_3d_geometry::cubeVertices;
	const std::vector<glm::vec3> pyramid = static_3d_geometry::pyramidVertices;
	//color
	const std::vector<glm::vec3> pyramidColor = static_3d_geometry::pyramidFaceColors;
	const std::vector<glm::vec3> cubeColor = static_3d_geometry::cubeFaceColors;




	Shader shader("./shaders/shader.vertex", "./shaders/shader.fragment");
	shader.Bind();


	//VAO (create once and use attributes to tell open gl wich buffer container which data (vertex buffer, color buffer,...) 
	VertexArray va;
	va.bind();

	//VB0 creation for position
	VertexBuffer VAOcube(cube);
	VertexBuffer VAOpyramid(pyramid);

	//VB0 creation for color
	VertexBuffer VAOpyramidColor(pyramidColor);
	VertexBuffer VAOcubeColor(cubeColor);


	Camera myCamera(myOpengGLWindow.windowWidth, myOpengGLWindow.windowHeight, glm::vec3(0.0f, 0.0f, 20.0f), 60.0f);

	glm::mat4  projectionMatrix = myCamera.getProjectionMatrix();
	glm::mat4 View = myCamera.getViewMatrix();

	// Render rotating cube in the middle
	auto modelMatrixCube = glm::mat4(1.0);
	modelMatrixCube = glm::rotate(modelMatrixCube, 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrixCube = glm::rotate(modelMatrixCube, 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	modelMatrixCube = glm::rotate(modelMatrixCube, 0.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
	glm::mat4 Model = modelMatrixCube;

	glm::mat4 mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

	shader.setUniformMat4f("MVP", mvp);

		 //Render 4 pyramids around the cube with the positions defined in the following array
	glm::vec3 pyramidTranslationVectors[] =
	{
		glm::vec3(-12.0f, 7.0f, 0.0f),
		glm::vec3(12.0f, 7.0f, 0.0f),
		glm::vec3(12.0f, -7.0f, 0.0f),
		glm::vec3(-12.0f, -7.0f, 0.0f)
	};
	glm::mat4 modelMatrixPyramid = glm::mat4(1.0);
	glEnable(GL_DEPTH_TEST);
	float currentAngle = 0;
	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);
	glfwSwapInterval(1);

    while (glfwGetKey(myOpengGLWindow.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(myOpengGLWindow.window)) {
		myOpengGLWindow.clearColorAndDepthBuffer();





		VAOpyramidColor.bind(1);//attribute 0 on gpu locaiton will be the vertex 3D location


		

		for (const auto& pyramidTranslation : pyramidTranslationVectors)
		{
			modelMatrixPyramid = glm::mat4(1.0);
			modelMatrixPyramid = glm::translate(modelMatrixPyramid, pyramidTranslation);
			modelMatrixPyramid = glm::rotate(modelMatrixPyramid, currentAngle, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixPyramid = glm::scale(modelMatrixPyramid, glm::vec3(3.0f, 3.0f, 3.0f));

			Model = modelMatrixPyramid;

			mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);



			VAOpyramid.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
			glDrawArrays(GL_TRIANGLES, 0, 4 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
			glDisableVertexAttribArray(0);

			VAOpyramid.unbind();


		


		};
		VAOpyramidColor.unbind();
		glDisableVertexAttribArray(1);

		 currentAngle += glm::radians(1.0f);

		 // Render rotating cube in the middle
		 auto modelMatrixCube = glm::mat4(1.0);
		 modelMatrixCube = glm::rotate(modelMatrixCube, 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));
		 modelMatrixCube = glm::rotate(modelMatrixCube, currentAngle, glm::vec3(0.0f, 1.0f, 0.0f));
		 modelMatrixCube = glm::rotate(modelMatrixCube, currentAngle, glm::vec3(0.0f, 0.0f, 1.0f));
		 modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));

		 glm::mat4 mvp = projectionMatrix * View * modelMatrixCube; // Remember, matrix multiplication is the other way around

		 shader.setUniformMat4f("MVP", mvp);

		 VAOcubeColor.bind(1);

		 VAOcube.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
		 glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
		 glDisableVertexAttribArray(0);
		 VAOcube.unbind();
		 VAOcubeColor.unbind();
		 glDisableVertexAttribArray(1);

//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


		// Swap buffers
		glfwSwapBuffers(myOpengGLWindow.window);
		glfwPollEvents();

    }

    glfwTerminate();

    return 0;

}