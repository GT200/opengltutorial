#pragma once
#include <glm/glm.hpp>;
#include "shader.h"
class Camera
{
public:
	Camera(float screenWidth, float screenHeight, glm::vec3 location, float Fov);
	void computeView(float screenWidth, float screenHeight);

	void setFoV(float newFoV);

	glm::mat4& getViewMatrix();

	glm::mat4& getProjectionMatrix();

	glm::vec3 location;


private:
	float Fov;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

};



