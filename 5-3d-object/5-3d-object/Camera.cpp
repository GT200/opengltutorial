#include "Camera.h"
#include <glm/gtx/transform.hpp>

Camera::Camera(float screenWidth, float screenHeight, glm::vec3 location, float Fov)
{
	this->location = location;
	this->Fov = Fov;
	this->computeView(screenWidth, screenHeight);
}

void Camera::computeView(float screenWidth, float screenHeight)
{
	// Projection matrix display range : 0.1 unit < -> 100 units
	this->projectionMatrix = glm::perspective(
		(float)glm::radians(this->Fov),
		(float)((float)screenWidth / (float)screenHeight),
		0.5f,
		100.0f
	);
	// Camera matrix

	this->viewMatrix = glm::lookAt(
		this->location,           // Camera is here
		glm::vec3(0.0f, 0.0f, 0.0f), // and looks here : at the same position, plus "direction"
		glm::vec3(0.0f, 1.0f, 0.0f)                  // Head is up (set to 0,-1,0 to look upside-down)
	);

}


void Camera::setFoV(float newFoV)
{
	this->Fov = newFoV;
}

glm::mat4& Camera::getViewMatrix()
{
	return this->viewMatrix;
}

glm::mat4& Camera::getProjectionMatrix()
{
	return this->projectionMatrix;
}

