#ifndef RENDERER_H
#define RENDERER_H

#include <GL/glew.h>
#include <iostream>
#include <signal.h>

#include "vertexarray.h"
#include "shader.h"
#include "object.h"

#define ASSERT(x) if (!(x)) raise(SIGTRAP);



class Renderer
{
public:
    Renderer();
    void Draw(const VertexArray& va, const Object& o, const Shader& shader) const;
    void Clear() const;
};

#endif // RENDERER_H
