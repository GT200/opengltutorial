#include "HeightMap.h"
#include <glm/gtx/string_cast.hpp>
HeightMap::HeightMap()
{

}

#include "HeightMap.h"
std::vector<std::vector<float>> HeightMap::getHeightDataFromImageUsingGdal(const char* fileName)
{
    // Register all gdal drivers
    GDALAllRegister();

    GDALDataset* poDataset;

    // load a "dataset" 
    poDataset = (GDALDataset*)GDALOpen(fileName, GA_ReadOnly);

    // error handing!
    if (poDataset == NULL)
    {
        std::cout << "Failure to load file due to not existing or write permissions!!!" << std::endl;
        //return false;
        exit(1);
    }

    // time to find the width of the data and print it for sanity
    std::cout << "Data size: " << GDALGetRasterXSize(poDataset) << " " << GDALGetRasterYSize(poDataset) << std::endl;

    // Get the first band -- that's what this function does to grab one band.
    GDALRasterBand* poBand;
    int             nBlockXSize, nBlockYSize;
    int             bGotMin, bGotMax;
    double          MinMaxArray[2];
    poBand = poDataset->GetRasterBand(1);
    poBand->GetBlockSize(&nBlockXSize, &nBlockYSize);

    // Print some more handy information
    printf("Block=%dx%d Type=%s, ColorInterp=%s\n",
        nBlockXSize, nBlockYSize,
        GDALGetDataTypeName(poBand->GetRasterDataType()),
        GDALGetColorInterpretationName(
            poBand->GetColorInterpretation()));
    // something to hold our bin pixel values 
    float* pafScanline;

    // Get the min and max
    poBand->GetMinimum(&bGotMin);
    poBand->GetMaximum(&bGotMax);

    // this guy will look at your current band and compute min max or you can do the above
    if (!(bGotMin && bGotMax)) {
        GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, MinMaxArray);
    }

    double min = MinMaxArray[0];
    double max = MinMaxArray[1];

    // dimensions of our datasets
    int widthMNT = poBand->GetXSize();
    int heightMNT = poBand->GetYSize();

    this->rasterwidth = widthMNT;
    this->rasterheight = heightMNT;

    // load the data
    pafScanline = (float*)CPLMalloc(sizeof(float) * widthMNT * heightMNT);

    std::vector<std::vector<float>> imageMatrix = std::vector<std::vector<float>>(widthMNT, std::vector<float>(heightMNT, 0));

    auto err = poBand->RasterIO(GF_Read, 0, 0, widthMNT, heightMNT, pafScanline, widthMNT, heightMNT, GDT_Float32, 0, 0);
    std::cout << "Loaded data with status " << err << std::endl;

    // move everything to a vector -- slow memory but okay for what we are doing
    for (int i = 0; i < heightMNT; i++)
    {
        for (int j = 0; j < widthMNT; j++)
        {
            if (pafScanline[(i)*widthMNT + j] > 0) {
                imageMatrix[j][i] = pafScanline[(i)*widthMNT + j];
                //std::cMNTvector << pafScanline[(i)*width + j] << std::endl;
            }
            else
                imageMatrix[j][i] = 0;
        }
    }

    this->maxHeight = max;

    std::cout << "Done Loading into vector" << std::endl;
    //std::cMNTvector << (min) << max;
    std::cout << imageMatrix.size();
    std::cout << imageMatrix[0].size();
    CPLFree(pafScanline);
    return imageMatrix;
}


AllTerrainInformation HeightMap::generateTriangle(std::vector<std::vector<float>>& heightmap)
{

    // Our vertex information along with normals contained inside
    std::vector<glm::vec3> path;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> color;
    std::vector<glm::vec2> uvcoord;
    std::vector<glm::vec3> normals;





    int random_i_house = rand() % heightmap.size() - 10;         // v1 in the range 0 to 99
    int random_j_house = rand() % heightmap[1].size() - 10;     // v2 in the range 1 to 100

    int random_i_scraper = rand() % heightmap.size() - 10;         // v1 in the range 0 to 99
    int random_j_scraper = rand() % heightmap[1].size() - 10;     // v2 in the range 1 to 100


    // Lets do some index rendering because it will save us some memory to some degree
    std::vector<int> indexs = std::vector<int>();

    int xres = 1;
    int yres = 1;

    float xPosMax = 0;
    float yPosMax = 0;

    this->heightFactor = (this->maxHeight / 50);

    // Time to construct a height map based on the xres and yres for each group of four dots
    for (int i = 0; i < heightmap.size() - 1; i++)
    {

        for (int j = 0; j < heightmap[i].size() - 1; j++)
        {

            float UL = (float)(heightmap[i][j]) / heightFactor; // Upper left
            float LL = (float)(heightmap[i + 1][j]) / heightFactor; // Lower left
            float UR = (float)(heightmap[i][j + 1]) / heightFactor; // Upper right
            float LR = (float)(heightmap[i + 1][j + 1]) / heightFactor; // Lower right

            vertices.push_back({ i * xres, UL , j * yres });
            vertices.push_back({ (i + 1) * xres, LL, j * yres });
            vertices.push_back({ i * xres,UR, (j + 1) * yres });
            vertices.push_back({ (i + 1) * xres,LR , (j + 1) * yres });
            vertices.push_back({ (i + 1) * xres, LL , j * yres });
            vertices.push_back({ i * xres,UR, (j + 1) * yres });


            glm::vec3 ULV = { i * xres, UL * this->maxHeight, j * yres };
            glm::vec3 LLV = { (i + 1) * xres, LL * this->maxHeight, j * yres };
            glm::vec3 URV = { i * xres, UR * this->maxHeight, (j + 1) * yres };
            glm::vec3 LRV = { (i + 1) * xres, LR * this->maxHeight, (j + 1) * yres };

            // compute smoothed normal
            glm::vec3 a = ComputeNormal(ULV, i, j, heightmap.size(), heightmap[i].size(), heightmap, this->maxHeight,  xres, yres);
            glm::vec3 bb = ComputeNormal(LLV, i + 1, j, heightmap.size(), heightmap[i].size(), heightmap, this->maxHeight,  xres, yres);
            glm::vec3 c = ComputeNormal(URV, i, j + 1, heightmap.size(), heightmap[i].size(), heightmap, this->maxHeight,  xres, yres);
            glm::vec3 d = ComputeNormal(LRV, i + 1, j + 1, heightmap.size(), heightmap[i].size(), heightmap, this->maxHeight, xres, yres);

            
            normals.push_back(a);
            normals.push_back(bb);
            normals.push_back(c);
            normals.push_back(d);
            normals.push_back(bb);
            normals.push_back(c);

            if (heightmap[i][j] == this->maxHeight) {
                xPosMax = (float)i;
                yPosMax = (float)j;
            }

            if (i == j) {
                path.push_back({ i * xres, (heightmap[i][j] / heightFactor) + 5 , j * yres });
            }

            float r = rand() % 256 / 255.0f * heightmap[i][j] / this->maxHeight;
            float g = rand () % 256 / 255.0f * heightmap[i][j] / this->maxHeight;
            float b = 0.7f;


            if (heightmap[i][j] < this->maxHeight / 5) 
            {
                color.push_back(glm::vec3(222 / 255, 45 / 255, 38 / 255));
                color.push_back(glm::vec3(222 / 255, 45 / 255, 38 / 255));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));

            }

            else if (heightmap[i][j] > this->maxHeight / 5  && heightmap[i][j] < this->maxHeight / 4)
            {
                color.push_back(glm::vec3(229 / 255, 245 / 255, 249 / 255));
                color.push_back(glm::vec3(229 / 255, 245 / 255, 249 / 255));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));

            }


            else if (heightmap[i][j] > this->maxHeight / 4 && heightmap[i][j] < this->maxHeight / 3)
            {
                color.push_back(glm::vec3(44 / 255, 162 / 255, 95 / 255));
                color.push_back(glm::vec3(44 / 255, 162 / 255, 95 / 255));
                color.push_back(glm::vec3(r, g, b));

                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
            }

            else if (heightmap[i][j] > this->maxHeight / 3 && heightmap[i][j] < this->maxHeight / 2)
            {


                color.push_back(glm::vec3(116 / 255, 189 / 255, 219 / 255));
                color.push_back(glm::vec3(116 / 255, 189 / 255, 219 / 255));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
            }

            else if (heightmap[i][j] > this->maxHeight / 2 && heightmap[i][j] < this->maxHeight / 1)
            {


                color.push_back(glm::vec3(43 / 255, 140 / 255, 190 / 255));
                color.push_back(glm::vec3(43 / 255, 140 / 255, 190 / 255));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
                color.push_back(glm::vec3(r, g, b));
            }





            //color.push_back(glm::vec3(r, g, b));

            // Push back vector information for these group of dots
            uvcoord.push_back(glm::vec2((float)i / (float)heightmap.size(), (float)(j + 1) / (float)heightmap[i].size()));
            uvcoord.push_back(glm::vec2((float)(i + 1) / (float)heightmap.size(), (float)(j + 1) / (float)heightmap[i].size()));
            uvcoord.push_back(glm::vec2((float)(i + 1) / (float)heightmap.size(), (float)j / (float)heightmap[i].size()));
            uvcoord.push_back(glm::vec2((float)i / (float)heightmap.size(), (float)(j + 1) / (float)heightmap[i].size()));
            uvcoord.push_back(glm::vec2((float)i / (float)heightmap.size(), (float)j / (float)heightmap[i].size()));
            uvcoord.push_back(glm::vec2((float)(i + 1) / (float)heightmap.size(), (float)j / (float)heightmap[i].size()));


            indexs.push_back(vertices.size() - 1);
            indexs.push_back(vertices.size() - 2);
            indexs.push_back(vertices.size() - 4);
            indexs.push_back(vertices.size() - 3);
            //indexs.push_back(vertices.size() - 1);

            if (i % 50 == 0 && j%100 == 0)
            {
                this->housesLocation.push_back(glm::vec3(random_i_house, (heightmap[random_i_house][random_j_house] / heightFactor) - 1, random_j_house));
                this->movingObjectLocation.push_back(glm::vec3(random_i_house, (heightmap[random_i_house][random_j_house] / heightFactor) - 1, random_j_house));
                random_i_house = rand() % heightmap.size();         // v1 in the range 0 to 99
                random_j_house = rand() % heightmap[i].size();
            }

            if (i % 70 == 0 && j % 200 == 0)
            {
                random_i_scraper = rand() % heightmap.size();         // v1 in the range 0 to 99
                random_j_scraper = rand() % heightmap[i].size();
                this->skyscraperLocation.push_back(glm::vec3(random_i_scraper, (heightmap[random_i_scraper][random_j_scraper] / heightFactor) - 1, random_j_scraper));
            }




        }
    }

    //path = zigZagMatrix(heightmap, heightmap.size(), heightmap[1].size());

    AllTerrainInformation TerrainInformation;

    //findPaths(heightmap, path, 0, 0);

    TerrainInformation.path = path;
    TerrainInformation.vertices = vertices;
    TerrainInformation.color = color;
    TerrainInformation.uvcoord = uvcoord;
    TerrainInformation.normals = normals;
    TerrainInformation.xPosMax = xPosMax;
    TerrainInformation.yPosMax = yPosMax;

    return TerrainInformation;
}

std::vector<std::vector<glm::vec2>> HeightMap::setUpTextureCoordinates()
{
    std::vector<std::vector<glm::vec2>> textureCoordinates= std::vector<std::vector<glm::vec2>>(this->row, std::vector<glm::vec2>(this->col));

    const auto textureStepU = 0.1f;
    const auto textureStepV = 0.1f;

    for (auto i = 0; i < this->row; i++)
    {
        for (auto j = 0; j < this->col; j++) {
            textureCoordinates[i][j] = glm::vec2(textureStepU * j, textureStepV * i);
        }
    }

    return textureCoordinates;
}

glm::vec3 HeightMap::randomNextStep(std::vector<std::vector<float>> arr, int x, int y, int maxX ,int maxY)
{
    int indice = rand() % 4;
    //std::cout << indice;
    std::vector<glm::vec2> nextPointToVisite = {
        //glm::vec2(x - 1, y - 1),
        glm::vec2(x - 1, y),
        //glm::vec2(x - 1, y + 1),
        glm::vec2(x, y + 1),
        //glm::vec2(x + 1, y + 1),
        glm::vec2(x + 1, y),
        //glm::vec2(x + 1, y - 1),
        glm::vec2(x, y - 1)
    };
    int next_x = nextPointToVisite[indice].x;
    int next_y = nextPointToVisite[indice].y;

    if (next_x >= maxX)
    {
        next_x--;
    }
    else if (next_x <=0)
    {
        next_x++;
    }
    if (next_y <= 0)
    {
        next_y++;
    }
    else if (next_y >= maxY)
    {
        next_y--;
    }

    //std::cout << (arr[next_x][next_y])<<"\n";
    

    return glm::vec3(next_x, arr[next_x][next_y]/this->heightFactor, next_y);
}


glm::vec3 HeightMap::ComputeNormal(glm::vec3 center, int i, int j, int width, int height, std::vector<std::vector<float>>& data, float Max, float xres, float yres)
{
    // Compute center of all values which is the i and j passed in
    glm::vec3 left;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 down;
    glm::vec3 sum = glm::vec3(0, 0, 0);
    bool l = false;
    bool r = false;
    bool u = false;
    bool d = false;

    int count = 0;
    // Compute left
    if (i - 1 >= 0)
    {

        left = glm::vec3((i - 1) * xres, data[i - 1][j], j * yres);
        left = center - left;
        l = true;
    }

    // Compute right
    if (i + 1 < width)
    {
        right = glm::vec3((i + 1) * xres, data[i + 1][j], j * yres);
        right = center - right;
        r = true;
    }

    // Compute up
    if (j - 1 >= 0)
    {
        up = glm::vec3((i)*xres, data[i][j - 1], (j - 1) * yres);
        up = center - up;
        u = true;
    }

    // Compute down
    if (j + 1 < height)
    {
        down = glm::vec3((i)*xres, data[i][j + 1], (j + 1) * yres);
        down = center - down;
        d = true;
    }

    // Compute normals
    if (u && r)
    {
        glm::vec3 v1 = cross(up, right);
        if (v1.y < 0)
        {
            v1 *= -1;
        }
        sum += v1;
        count = count + 1;
    }
    if (u && l)
    {
        glm::vec3 v1 = cross(up, left);
        if (v1.y < 0)
        {
            v1 *= -1;
        }
        sum += v1;
        count = count + 1;
    }
    if (d && r)
    {
        glm::vec3 v1 = cross(down, right);
        if (v1.y < 0)
        {
            v1 *= -1;
        }
        sum += v1;
        count = count + 1;
    }
    if (d && l)
    {
        glm::vec3 v1 = cross(down, left);
        if (v1.y < 0)
        {
            v1 *= -1;
        }
        sum += v1;
        count = count + 1;
    }

    // Compute average normal
    sum /= count;
    //auto t = normalize(sum);

    // Normalize it and return :D!!!! Enjoy your smoothed normal for some smooth shading!
    return glm::normalize(sum);
};

