#pragma once
#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include <ogrsf_frmts.h>




struct AllTerrainInformation
{
	std::vector<glm::vec3> path;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> color;
	std::vector<glm::vec2> uvcoord;
	std::vector<glm::vec3> normals;
	float xPosMax;
	float yPosMax;

};


class HeightMap
{
public:
	HeightMap(); 
	std::vector<std::vector<float>> getHeightDataFromImageUsingGdal(const char* fileName);
	AllTerrainInformation generateTriangle(std::vector<std::vector<float>> &heightmap);
	std::vector<std::vector<glm::vec2>> setUpTextureCoordinates();
	glm::vec3 ComputeNormal(glm::vec3 center, int i, int j, int width, int height, std::vector<std::vector<float>>& data, float Max, float xres, float yres);
	int row;
	int col;

	float xPosMax;
	float yPosMax;
	float maxHeight;
	float heightFactor;
	float rasterwidth;
	float rasterheight;


	std::vector<glm::vec3> housesLocation;
	std::vector<glm::vec3> skyscraperLocation;
	std::vector<glm::vec3> movingObjectLocation;
	

	glm::vec3 randomNextStep(std::vector<std::vector<float>> arr, int x, int y, int maxX, int maxY);
};

