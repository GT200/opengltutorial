#ifndef CAMERA_H
#define CAMERA_H

#include "glm/glm.hpp"
#include "glm/gtx/transform.hpp"
#include "shader.h"

class Camera
{
public:
    Camera(float width, float height);
    void computeMatrices(float width, float height);

    glm::vec3 position;

    float horizontalAngle;

    float verticalAngle;

    void setFoV(float newFoV);

    const glm::mat4 &getViewMatrix() const;

    const glm::mat4 &getProjectionMatrix() const;

    void Bind(Shader *shader);

    float speed;

    float jumpStratTime = 0;

    float jumpLenTime = 2;

    float jumbHeight = 1;

    float jumpPositionX = 0;
    float jumpPositionY = 0;
    float jumpPositionK = 0;
    float initialJumpPositiony;


    void jump(float time);
    void CupdateJump(float time, float floatGroudHeigt);

private:
    float FoV;
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;
};

#endif // CAMERA_H
