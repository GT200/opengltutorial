﻿#include "OpenGLWindow.h"
#include "SimpleStaticShape.h"
#include "VertexArray.h"
#include "VertexBuffer.h"

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include "FPSControls.h"
#include "UvBuffer.h"

#include "HeightMap.h"
#include"Object.h"
#include "navigationcontrols.h"
#include <glm/gtx/string_cast.hpp>

int main()
{

	OpenGLWindow myOpengGLWindow;
	myOpengGLWindow.initializeGLFW();
	myOpengGLWindow.windowHint();
	myOpengGLWindow.createOpenGlWindow("My first window");
	myOpengGLWindow.initializeSceneColor();
	myOpengGLWindow.initializeGlew();//glew is nedeed for using higher level of opengl

	VertexArray va;
	va.Bind();
	Shader shader("./shaders/shader.vertex", "./shaders/shader.fragment");
	shader.Bind();

	

	const char* mntFile = "./img/Christchurch-H100.tif";
	HeightMap myHeightMap;
	std::vector<std::vector<float>> dem = myHeightMap.getHeightDataFromImageUsingGdal(mntFile);
	AllTerrainInformation verticesInfo = myHeightMap.generateTriangle(dem);

	Camera myCamera(myOpengGLWindow.windowWidth, myOpengGLWindow.windowHeight);
	myCamera.position = glm::vec3(42.539864, 1.412776, 109.368530) + glm::vec3(0, 0, 5);
	myCamera.setFoV(80);
	//FPSControls controls(myOpengGLWindow.window, &myCamera);
	NavigationControls controls(myOpengGLWindow.window, &myCamera);


	glm::vec3 positionTerrain(-10.0f, -10.0f, 0.0f);
	glm::vec3 rotation(0.0f, 0.0f, 0.0f);

	Object myTerrain(verticesInfo.vertices, verticesInfo.uvcoord, "./img/R.jpg");
	myTerrain.position = glm::vec3(0, 0, 0);
	myTerrain.rotationAngles = glm::vec3(0, 0, 90);
	//Object myTerrain(verticesInfo.vertices, "./img/grass.jpg", verticesInfo.uvcoord, positionTerrain, rotation);
	VertexBuffer VAOTerrainColor(verticesInfo.color);
	VertexBuffer VAOTerrainNormals(verticesInfo.normals);


	glm::mat4 View = myCamera.getViewMatrix();
	glm::mat4 projectionMatrix = myCamera.getProjectionMatrix();
	glm::mat4 Model = myTerrain.getModelMatrix();
	glm::mat4 mvp = projectionMatrix * View * Model;
	shader.setUniformMat4f("MVP", mvp);

	Object myCube(static_3d_geometry::cubeVertices, static_3d_geometry::cubeTexCoords, "./img/prismarine_dark.png");
	Object mypyramid(static_3d_geometry::pyramidVertices, static_3d_geometry::pyramidTexCoords, "./img/wood.jpg");
	VertexBuffer VAOPColor(static_3d_geometry::pyramidFaceColors);

	myCube.position = glm::vec3(0, 0, 0);
	VertexBuffer VAOcubeColor(static_3d_geometry::cubeFaceColors);

	Object myCube2(static_3d_geometry::cubeVertices, static_3d_geometry::cubeTexCoords, "./img/container.jpg");




	float lastTime = glfwGetTime();
	float currentTime, deltaTime;

	float rotationAngleRad_house = 0;
	float rotationAngleRad_scraper = 0;
	int path_index = 0;
	std::vector<glm::vec3> path = verticesInfo.path;
	//controls.setSpeed(0.0001);

	double lastTimeCubeSpeed = glfwGetTime();
	double nowTimeCubeSpide = 0;

	myCamera.speed = 1;

    while (glfwGetKey(myOpengGLWindow.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(myOpengGLWindow.window)) {
		currentTime = glfwGetTime();

		//shader.setUniform3fv("lightPos", glm::vec3(42.539864,50, 109.368530));
		shader.setUniform3fv("lightPos", glm::vec3(500 - path_index, 200, 500 -path_index));

		shader.setUniform3fv("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));


		deltaTime = currentTime - lastTime;
		lastTime = currentTime;
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		controls.update(deltaTime, &shader);
		controls.update(deltaTime, myCamera.position.y);

		float floatGroudHeigt = dem[floor(myCamera.position.x)][floor(myCamera.position.z)] / myHeightMap.heightFactor;
		
		if (myCamera.jumpStratTime != 0)
		{
			myCamera.CupdateJump(deltaTime, floatGroudHeigt);
		}

		myCamera.computeMatrices(myOpengGLWindow.windowWidth, myOpengGLWindow.windowHeight);
		myOpengGLWindow.clearColorAndDepthBuffer();

		float d = (glm::distance(
			myCamera.position,
			glm::vec3(myCamera.position.x, floatGroudHeigt, myCamera.position.z)));

		float broder_limit = 5;
		if (myCamera.position.x < broder_limit) {
			myCamera.position.x = broder_limit;
		}
		if (myCamera.position.z < broder_limit) {
			myCamera.position.z = broder_limit;
		}
		if (myCamera.position.x > myHeightMap.rasterheight - broder_limit) {
			myCamera.position.x = myHeightMap.rasterheight - broder_limit;
		}
		if (myCamera.position.z > myHeightMap.rasterwidth - broder_limit) {
			myCamera.position.z = myHeightMap.rasterwidth - broder_limit;
		}

		if (d < 3) 
		{
			myCamera.position = glm::vec3(myCamera.position.x, floatGroudHeigt + 5, myCamera.position.z);
		}

		if (d > 50)
		{
			myCamera.position = glm::vec3(myCamera.position.x, floatGroudHeigt + 35, myCamera.position.z);
		}

		View = myCamera.getViewMatrix();
		projectionMatrix = myCamera.getProjectionMatrix();
		Model = glm::mat4(1.0);
		mvp = projectionMatrix * View * Model;

		mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

		shader.setUniformMat4f("MVP", mvp);

		//myTerrain.bindWithTexture(0, 2, 0);
		VAOTerrainColor.Bind(2);
		VAOTerrainNormals.Bind(3);
		myTerrain.Bind();
		myTerrain.Draw();
		myTerrain.Unbind();
		VAOTerrainNormals.Unbind();
		VAOTerrainColor.Unbind();

		
		Model = myCube2.getModelMatrix();
		Model = glm::scale(Model, glm::vec3(5, 5, 5));
		mvp = projectionMatrix * View * Model;

		mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

		shader.setUniformMat4f("MVP", mvp);

		//std::cout << glm::to_string(myCamera.position) << std::endl;


		nowTimeCubeSpide = glfwGetTime();
		double dtCube = nowTimeCubeSpide - lastTimeCubeSpeed;
		if (dtCube >=0.2)
		{
			myCube2.position = path[path_index];
			path_index += 1;
			//myCamera.position = myCube.position + glm::vec3(0, -4, 5);
			lastTimeCubeSpeed = nowTimeCubeSpide;

			if (path_index == path.size()-2)
			{
				path_index = 0;
			}
				
		}

		//myTerrain.bindWithTexture(0, 2, 0);
		VAOcubeColor.Bind(2);
		myCube2.Bind();
		myCube2.Draw();
		myCube2.Unbind();
		VAOcubeColor.Unbind();



		for (auto i = 0; i < myHeightMap.movingObjectLocation.size(); i++)
		{
			// First, calculate the basic position of house
			auto modelMatrixMovingObj = glm::mat4(1.0);
			modelMatrixMovingObj = glm::translate(modelMatrixMovingObj, myHeightMap.movingObjectLocation[i]);
			modelMatrixMovingObj = glm::scale(modelMatrixMovingObj, glm::vec3(1, 10, 1));

			Model = modelMatrixMovingObj;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			glm::mat4 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);
			//myTerrain.bindWithTexture(0, 2, 0);
			myCube.Bind();
			myCube.Draw();
			myCube.Unbind();
	
			myHeightMap.movingObjectLocation[i] = myHeightMap.randomNextStep(dem, floor(myHeightMap.movingObjectLocation[i].x), floor(myHeightMap.movingObjectLocation[i].z), myHeightMap.rasterwidth, myHeightMap.rasterheight) + glm::vec3(0, 5, 0) * (deltaTime/100.0f);
		
			
		}
		


		for (auto i = 0; i < myHeightMap.housesLocation.size(); i++)
		{
			// Lets' predefine some sizes
			const auto houseBottomSize = 10.0f;
			const auto roofTopSize = 12.0f;

			// First, calculate the basic position of house
			auto modelMatrixHouse = glm::mat4(1.0);
			modelMatrixHouse = glm::translate(modelMatrixHouse, myHeightMap.housesLocation[i]);
			//std::cout << glm::to_string(myHeightMap.housesLocation[i]) << "\n";

			// Render bottom cube of the house
			glm::mat4 modelMatrixBottom = glm::translate(modelMatrixHouse, glm::vec3(0.0f, houseBottomSize / 2.0f, 0.0f));
			modelMatrixBottom = glm::rotate(modelMatrixBottom, rotationAngleRad_house, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixBottom = glm::scale(modelMatrixBottom, glm::vec3(houseBottomSize, houseBottomSize, houseBottomSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixBottom;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			glm::mat4 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);
			//myTerrain.bindWithTexture(0, 2, 0);
			VAOcubeColor.Bind(2);
			myCube.Bind();
			myCube.Draw();
			myCube.Unbind();
			VAOcubeColor.Unbind();


			// Render top (roof) of the house
			const auto translateTopY = houseBottomSize + roofTopSize / 2.0f - 1.0f;
			glm::mat4 modelMatrixTop = glm::translate(modelMatrixHouse, glm::vec3(0.0f, translateTopY, 0.0f));
			modelMatrixTop = glm::rotate(modelMatrixTop, rotationAngleRad_house, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixTop = glm::scale(modelMatrixTop, glm::vec3(roofTopSize, roofTopSize, roofTopSize));
			Model = modelMatrixTop;

			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;


			mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);



			VAOPColor.Bind(2);

			mypyramid.Bind();//attribute 0 on gpu locaiton will be the vertex 3D location
			mypyramid.Draw();
			mypyramid.Unbind();
			VAOPColor.Unbind();


		};

		// Render "skyscrapers" on the right
		for (auto i = 0; i < myHeightMap.skyscraperLocation.size(); i++)
		{
			const auto houseBottomSize = 10.0f;
			const auto houseMiddleSize = 7.0f;
			const auto houseTopSize = 4.0f;

			// First, calculate the basic position of skyscraper
			auto modelMatrixHouse = glm::mat4(1.0);
			modelMatrixHouse = glm::translate(modelMatrixHouse, myHeightMap.skyscraperLocation[i]);

			// Render the bottom part of skyscraper
			glm::mat4 modelMatrixBottom = glm::translate(modelMatrixHouse, glm::vec3(0.0f, houseBottomSize / 2.0f, 0.0f));
			modelMatrixBottom = glm::rotate(modelMatrixBottom, rotationAngleRad_house, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixBottom = glm::scale(modelMatrixBottom, glm::vec3(houseBottomSize, houseBottomSize, houseBottomSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixBottom;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			glm::mat4 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);

			//myTerrain.bindWithTexture(0, 2, 0);
			VAOcubeColor.Bind(2);
			myCube.Bind();
			myCube.Draw();
			myCube.Unbind();
			VAOcubeColor.Unbind();;

			// Render the middle part of skyscraper
			const auto translateMiddleY = houseBottomSize + houseMiddleSize / 2.0f;
			glm::mat4 modelMatrixMiddle = glm::translate(modelMatrixHouse, glm::vec3(0.0f, translateMiddleY, 0.0f));
			modelMatrixMiddle = glm::rotate(modelMatrixMiddle, rotationAngleRad_scraper, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixMiddle = glm::scale(modelMatrixMiddle, glm::vec3(houseMiddleSize, houseMiddleSize, houseMiddleSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixMiddle;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);

			//myTerrain.bindWithTexture(0, 2, 0);
			VAOcubeColor.Bind(2);
			myCube.Bind();
			myCube.Draw();
			myCube.Unbind();
			VAOcubeColor.Unbind();;
			// Render the top part of skyscraper
			const auto translateTopY = houseMiddleSize + houseBottomSize + houseTopSize / 2.0f;
			glm::mat4 modelMatrixTop = glm::translate(modelMatrixHouse, glm::vec3(0.0f, translateTopY, 0.0f));
			modelMatrixTop = glm::rotate(modelMatrixTop, rotationAngleRad_scraper, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixTop = glm::scale(modelMatrixTop, glm::vec3(houseTopSize, houseTopSize, houseTopSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixTop;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around


			shader.setUniformMat4f("MVP", mvp);

			//myTerrain.bindWithTexture(0, 2, 0);
			VAOcubeColor.Bind(2);
			myCube.Bind();
			myCube.Draw();
			myCube.Unbind();
			VAOcubeColor.Unbind();
		}
		rotationAngleRad_scraper += 0.3;

		// Swap buffers
		glfwSwapBuffers(myOpengGLWindow.window);
		glfwPollEvents();

    }

    glfwTerminate();

    return 0;

}