## OpenGL_Project: 

## Instllation: 
# Lunix: utiliser cmakefifile et installer les autres librariry avec apt get install (glew, glm, glfw, gdal)
 - installation specifique pour gdal: 

  *installation: 
      - sudo add-apt-repository ppa:ubuntugis/ppa && sudo apt-get update

      -sudo apt-get update

      -sudo apt-get install gdal-bin

      -sudo apt-get install libgdal-dev

      -export CPLUS_INCLUDE_PATH=/usr/include/gdal

      -export C_INCLUDE_PATH=/usr/include/gdal

      -gdalinfo --version

    *cmakefile config: 

      # find system installed GDAL package with predefined CMake variable for finding GDAL

      find_package(GDAL REQUIRED)

# Windows: utiliser visual studio et installer VCPKG package manager pour installer les autres librairie (glew, glm, glfw, gdal)
  - installer visual studio community 
  - installer (cloner) vcpkage à la racine du disque et suivre la doc installation sur le lien suivant https://vcpkg.io/en/getting-started.html
    - git clone https://github.com/Microsoft/vcpkg.git (racine C)
    - renter dans le dosier vcpkg
    - .\vcpkg\bootstrap-vcpkg.bat
    - vcpkg integrate install (pour coupler visual studio et vcpkg)
    - example d' installation de librairie: vcpkg install glm:x64-windows, glew:x64-windows, glfw3:x64-windows, gdal:x64-windows


## Fonctionalité (video available)

# MNT: 
  - image avec la hauteur 
  - les vertex sont mis en place de manière classic (2 triangles pour former un carrée) (les index aurrait été mieux pas eu le temps)
  - pour le shading, la normale est aussi calculer
  - pour éclairer la scène, la lumière ambiante et diffuse à été mis en place 
  - quelque maison et autre objets on été ajouter à la scène 

# Camera:
  - Elle représente une personnage
  - Elle ne peut pas traverser le mnt (le mnt est le sol)
  - Elle ne peut pas s'éloigner en haut è une distance de 35 unité
  - Elle a une liberté de mouvment entre le mnt et la hauteur 35
  - Elle peut sauter sans dépasser la distance 35m du sol 

# Cube 2: 
  - trajectoire diagolnale par rapport au mnt 
  - suis la hauteur du mammnt 
  - trajectoire refait en boucle 

# les Rectangles (peuple du village): 
  - Tranjectoire aléatoire: random walk
  



