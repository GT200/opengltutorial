#pragma once
#include "Controls.h"

class FPSControls : public Controls
{
public:
    FPSControls(GLFWwindow* window, Camera* camera);
    void update(float deltaTime, Shader* shader);
};


