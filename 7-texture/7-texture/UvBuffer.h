#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <gl/glew.h>

class UvBuffer
{
public:
	UvBuffer(std::vector<glm::vec2>& uvCoordinates);
	~UvBuffer();
	void bind(int indexForFragmentShader);
	void unbind();
	unsigned int getBitSize() { return this->bitSizeOfverticesData; };
	unsigned int getNbVertices() { return this->nb_vertices; };

	GLuint UvBufferId;
	GLuint nb_vertices;
	GLuint bitSizeOfverticesData;
};

