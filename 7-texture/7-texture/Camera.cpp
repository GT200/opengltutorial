#include "Camera.h"
#include <glm/gtx/transform.hpp>

Camera::Camera(float screenWidth, float screenHeight, glm::vec3 location, float horizontalAngle, float verticalAngle, float Fov)
{
	this->location = location;
	this->horizontalAngle = horizontalAngle;
	this->verticalAngle = verticalAngle;
	this->Fov = Fov;
	this->computeView(screenWidth, screenHeight);
}

void Camera::computeView(float screenWidth, float screenHeight)
{
	glm::vec3 direction(cos(this->verticalAngle) * sin(this->horizontalAngle),
		sin(this->verticalAngle),
		cos(this->verticalAngle) * cos(this->horizontalAngle)
	);

	//right vector 
	glm::vec3 right = glm::vec3(
		sin(this->horizontalAngle - (3.14f / 2.0f)),
		0,
		cos(this->horizontalAngle - (3.14f / 2.0f))
	);

	// Up vector : perpendicular to both direction and right
	glm::vec3 up = glm::cross(right, direction);

	// Projection matrix display range : 0.1 unit < -> 100 units
	this->projectionMatrix = glm::perspective(
		(float)glm::radians(this->Fov),
		(float)((float)screenWidth / (float)screenHeight),
		0.1f,
		100.0f
	);
	// Camera matrix

	this->viewMatrix = glm::lookAt(
		this->location,           // Camera is here
		this->location + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
	);

}


void Camera::setFoV(float newFoV)
{
	this->Fov = newFoV;
}

glm::mat4& Camera::getViewMatrix()
{
	return this->viewMatrix;
}

glm::mat4& Camera::getProjectionMatrix()
{
	return this->projectionMatrix;
}

void Camera::bind(Shader* shader)
{
	shader->setUniform3fv("camPosition", this->location);
}

