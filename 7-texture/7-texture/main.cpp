﻿#include "OpenGLWindow.h"
#include "SimpleStaticShape.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include "FPSControls.h"
#include "Texture.h"
#include "UvBuffer.h"
#include "Terrain.h"
int main()
{
	OpenGLWindow myOpengGLWindow;
	myOpengGLWindow.initializeGLFW();
	myOpengGLWindow.windowHint();
	myOpengGLWindow.createOpenGlWindow("My first window");
	myOpengGLWindow.initializeSceneColor();
	myOpengGLWindow.initializeGlew();//glew is nedeed for using higher level of opengl

	VertexArray va;
	va.bind();
	Shader shader("./shaders/shader.vertex", "./shaders/shader.fragment");
	shader.Bind();






	//VB0 creation for position
	VertexBuffer VAOcube(static_3d_geometry::cubeVertices);
	VertexBuffer VAOpyramid(static_3d_geometry::pyramidVertices);
	VertexBuffer VAOground(static_3d_geometry::plainGroundVertices);


	//VB0 creation for color
	VertexBuffer VAOpyramidColor(static_3d_geometry::pyramidFaceColors);
	VertexBuffer VAOcubeColor(static_3d_geometry::cubeFaceColors);
	VertexBuffer VAOGroundColor(static_3d_geometry::plainGroundColors);


	Camera myCamera(myOpengGLWindow.windowWidth, myOpengGLWindow.windowHeight, glm::vec3(0, 0, 40), 3.14f, 0.5f, 80.0f);
	FPSControls controls(myOpengGLWindow.window, &myCamera);

	glm::mat4  projectionMatrix = myCamera.getProjectionMatrix();
	glm::mat4 View = myCamera.getViewMatrix();



		 //Render 4 pyramids around the cube with the positions defined in the following array
	glm::vec3 pyramidTranslationVectors[] =
	{
		glm::vec3(-12.0f, 7.0f, 0.0f),
		glm::vec3(12.0f, 7.0f, 0.0f),
		glm::vec3(12.0f, -7.0f, 0.0f),
		glm::vec3(-12.0f, -7.0f, 0.0f)
	};
	glm::mat4 modelMatrixPyramid = glm::mat4(1.0);
	glEnable(GL_DEPTH_TEST);
	float currentAngle = 0;
	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);
	//glfwSwapInterval(1);

	float lastTime = glfwGetTime();
	float currentTime, deltaTime;

	float rotationAngleRad = 0;


	const char* path = "container.jpg";

	 std::vector<glm::vec2> g_uv_buffer_data_Coord = {
	glm::vec2(0,1),
	glm::vec2(1,1),
	glm::vec2(1,0),
	glm::vec2(0,1),
	glm::vec2(0,0),
	glm::vec2(1,0)
		};

	UvBuffer uvs(g_uv_buffer_data_Coord);
	UvBuffer uvsp(static_3d_geometry::pyramidTexCoords);
	UvBuffer uvsc(static_3d_geometry::cubeTexCoords);




	//Texture t("img/brick.png");

	Texture tGround("img/clay.png");
	Texture tPyramid("img/prismarine_dark.png");
	Texture tCube("img/prismarine_dark.png");

	glm::mat4 Model = glm::mat4(1.0);
	glm::mat4 mvp = projectionMatrix * View * Model;


    while (glfwGetKey(myOpengGLWindow.window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(myOpengGLWindow.window)) {
		currentTime = glfwGetTime();
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;


		controls.update(deltaTime, &shader);
		myCamera.computeView(myOpengGLWindow.windowWidth, myOpengGLWindow.windowHeight);
		myOpengGLWindow.clearColorAndDepthBuffer();

		View = myCamera.getViewMatrix();
		projectionMatrix = myCamera.getProjectionMatrix();
		Model = glm::mat4(1.0);
		mvp = projectionMatrix * View * Model;

		mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

		shader.setUniformMat4f("MVP", mvp);


		VAOGroundColor.bind(1);

		VAOground.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
		uvs.bind(2);
		tGround.bind(0);


		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);
		VAOground.unbind();
		VAOGroundColor.unbind();
		glDisableVertexAttribArray(1);
		uvs.unbind();
		tGround.unbind();
		glDisableVertexAttribArray(2);



		VAOpyramidColor.bind(1);//attribute 0 on gpu locaiton will be the vertex 3D location
		//rotationAngleRad += glm::radians(1.0f);
		for (auto i = 0; i < 10; i++)
		{
			// Lets' predefine some sizes
			const auto houseBottomSize = 10.0f;
			const auto roofTopSize = 12.0f;

			// First, calculate the basic position of house
			auto modelMatrixHouse = glm::mat4(1.0);
			modelMatrixHouse = glm::translate(modelMatrixHouse, glm::vec3(-40.0f, 0.0f, -125.0f + i * 25.0f));

			// Render bottom cube of the house
			glm::mat4 modelMatrixBottom = glm::translate(modelMatrixHouse, glm::vec3(0.0f, houseBottomSize / 2.0f, 0.0f));
			modelMatrixBottom = glm::rotate(modelMatrixBottom, rotationAngleRad, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixBottom = glm::scale(modelMatrixBottom, glm::vec3(houseBottomSize, houseBottomSize, houseBottomSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixBottom;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			glm::mat4 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);

			VAOcubeColor.bind(1);

			VAOcube.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location

			uvsc.bind(2);
			tCube.bind(0);

			glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
			glDisableVertexAttribArray(0);
			uvsc.unbind();
			tCube.unbind();
			glDisableVertexAttribArray(2);
			VAOcube.unbind();
			VAOcubeColor.unbind();

			// Render top (roof) of the house
			const auto translateTopY = houseBottomSize + roofTopSize / 2.0f - 1.0f;
			glm::mat4 modelMatrixTop = glm::translate(modelMatrixHouse, glm::vec3(0.0f, translateTopY, 0.0f));
			modelMatrixTop = glm::rotate(modelMatrixTop, rotationAngleRad, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixTop = glm::scale(modelMatrixTop, glm::vec3(roofTopSize, roofTopSize, roofTopSize));
			Model = modelMatrixTop;

			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;


			mvp = projectionMatrix * View * Model; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);



			VAOpyramid.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location

			uvsp.bind(2);
			tPyramid.bind(0);

			glDrawArrays(GL_TRIANGLES, 0, 4 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
			glDisableVertexAttribArray(0);

			uvs.unbind();
			tGround.unbind();
			glDisableVertexAttribArray(2);

			VAOpyramid.unbind();

		};

		// Render "skyscrapers" on the right
		for (auto i = 0; i < 10; i++)
		{
			const auto houseBottomSize = 10.0f;
			const auto houseMiddleSize = 7.0f;
			const auto houseTopSize = 4.0f;

			// First, calculate the basic position of skyscraper
			auto modelMatrixHouse = glm::mat4(1.0);
			modelMatrixHouse = glm::translate(modelMatrixHouse, glm::vec3(40.0f, 0.0f, -125.0f + i * 25.0f));

			// Render the bottom part of skyscraper
			glm::mat4 modelMatrixBottom = glm::translate(modelMatrixHouse, glm::vec3(0.0f, houseBottomSize / 2.0f, 0.0f));
			modelMatrixBottom = glm::rotate(modelMatrixBottom, rotationAngleRad, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixBottom = glm::scale(modelMatrixBottom, glm::vec3(houseBottomSize, houseBottomSize, houseBottomSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixBottom;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			glm::mat4 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);

			VAOcubeColor.bind(1);

			VAOcube.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
			uvsc.bind(2);
			tCube.bind(0);

			glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
			glDisableVertexAttribArray(0);

			uvs.unbind();
			tGround.unbind();
			glDisableVertexAttribArray(2);
			VAOcube.unbind();
			VAOcubeColor.unbind();

			// Render the middle part of skyscraper
			const auto translateMiddleY = houseBottomSize + houseMiddleSize / 2.0f;
			glm::mat4 modelMatrixMiddle = glm::translate(modelMatrixHouse, glm::vec3(0.0f, translateMiddleY, 0.0f));
			modelMatrixMiddle = glm::rotate(modelMatrixMiddle, rotationAngleRad, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixMiddle = glm::scale(modelMatrixMiddle, glm::vec3(houseMiddleSize, houseMiddleSize, houseMiddleSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixMiddle;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);

			VAOcubeColor.bind(1);

			VAOcube.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
			uvsc.bind(2);
			tCube.bind(0);

			glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
			glDisableVertexAttribArray(0);

			uvs.unbind();
			tGround.unbind();
			glDisableVertexAttribArray(2);
			VAOcube.unbind();
			VAOcubeColor.unbind();

			// Render the top part of skyscraper
			const auto translateTopY = houseMiddleSize + houseBottomSize + houseTopSize / 2.0f;
			glm::mat4 modelMatrixTop = glm::translate(modelMatrixHouse, glm::vec3(0.0f, translateTopY, 0.0f));
			modelMatrixTop = glm::rotate(modelMatrixTop, rotationAngleRad, glm::vec3(0.0f, 1.0f, 0.0f));
			modelMatrixTop = glm::scale(modelMatrixTop, glm::vec3(houseTopSize, houseTopSize, houseTopSize));
			//modelMatrixCube = glm::scale(modelMatrixCube, glm::vec3(5.0f, 5.0f, 5.0f));
			Model = modelMatrixTop;
			View = myCamera.getViewMatrix();
			projectionMatrix = myCamera.getProjectionMatrix();
			mvp = projectionMatrix * View * Model;

			 mvp = projectionMatrix * View * mvp = projectionMatrix * View * Model;
			; // Remember, matrix multiplication is the other way around

			shader.setUniformMat4f("MVP", mvp);

			VAOcubeColor.bind(1);

			VAOcube.bind(0);//attribute 0 on gpu locaiton will be the vertex 3D location
			uvsc.bind(2);
			tCube.bind(0);

			glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
			glDisableVertexAttribArray(0);

			uvs.unbind();
			tGround.unbind();
			glDisableVertexAttribArray(2);
			VAOcube.unbind();
			VAOcubeColor.unbind();
		}






//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


		// Swap buffers
		glfwSwapBuffers(myOpengGLWindow.window);
		glfwPollEvents();

    }

    glfwTerminate();

    return 0;

}