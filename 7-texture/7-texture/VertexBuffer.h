#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <glm/glm.hpp>

class VertexBuffer
{
public:
	//VertexBuffer() = default;
	VertexBuffer(const std::vector<glm::vec3> &verticesData);
	 ~VertexBuffer();
	GLuint vetexBufferId;
	GLuint nb_vertices;
	GLuint bitSizeOfverticesData;

	void bind(int BufferIndexAttributForGPU);
	void unbind();
	GLuint getNbVertices();

};

