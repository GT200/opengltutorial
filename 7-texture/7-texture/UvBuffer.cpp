#include "UvBuffer.h"

UvBuffer::UvBuffer(std::vector<glm::vec2>& uvCoordinates)
{
    this->nb_vertices = uvCoordinates.size();
    this->bitSizeOfverticesData = sizeof(glm::vec3) * this->nb_vertices;

    glGenBuffers(1, &this->UvBufferId);
    glBindBuffer(GL_ARRAY_BUFFER, this->UvBufferId);
    glBufferData(GL_ARRAY_BUFFER, this->bitSizeOfverticesData, &uvCoordinates[0], GL_STATIC_DRAW);
}

UvBuffer::~UvBuffer() 
{
    glDeleteBuffers(1, &this->UvBufferId);
}

void UvBuffer::bind(int indexForFragmentShader){
    glEnableVertexAttribArray(indexForFragmentShader);
    glBindBuffer(GL_ARRAY_BUFFER, this->UvBufferId);
    glVertexAttribPointer(indexForFragmentShader,2,GL_FLOAT,GL_FALSE,0,(void*)0);
} 

void UvBuffer::unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
